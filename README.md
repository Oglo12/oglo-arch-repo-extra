# Oglo Arch Repository
OgloTheNerd's Arch Linux repository for software he did not make.

# How To Use
1. Add this to `/etc/pacman.conf`:
```
[oglo-arch-repo-extra]
SigLevel = Optional DatabaseOptional
Server = https://gitlab.com/Oglo12/$repo/-/raw/main/$arch
```

2. Run this command:
```
sudo pacman -Syy
```
